package com.epam.courses.controller;

import com.epam.courses.model.BuisnessLogic;

public class PriorityQueueController {

  private BuisnessLogic buisnessLogic;

  public PriorityQueueController() {
    buisnessLogic = new BuisnessLogic();
  }

  public void addElementToPriorityQueue(Comparable element) {
    buisnessLogic.addElementToPriorityQueue(element);
  }

  public Comparable peekElementToPriorityQueue() {
    return buisnessLogic.peekElementToPriorityQueue();
  }

  public Comparable poolElementToPriorityQueue() {
    return buisnessLogic.poolElementToPriorityQueue();
  }
}
