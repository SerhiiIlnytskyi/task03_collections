package com.epam.courses.model.deque;

import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;

public class MyDeque<E> implements Deque<E> {

  public Object[] queue;
  private int head;
  private int tail;
  private int size;
  private int capacity = 10;

  public MyDeque() {
    this.head = -1;
    this.tail = 0;
    size = 0;
    queue = new Object[capacity];
  }

  public boolean isEmpty() {
    return this.head == -1;
  }

  @Override
  public void addFirst(E e) {
    fixOverflowCapacity();
    if (head == -1) {
      head = 0;
    } else if (head == 0) {
      head = capacity - 1;
    } else {
      head -= 1;
    }
    queue[head] = e;
    size++;
  }

  @Override
  public void addLast(E e) {
    fixOverflowCapacity();
    if (head == -1) {
      tail = 0;
    }
    if (tail == capacity - 1) {
      tail = 0;
    } else {
      tail = tail + 1;
    }
    queue[tail] = e;
    size++;
  }

  @Override
  public boolean offerFirst(E e) {
    addFirst(e);
    return queue[head].equals(e);
  }

  @Override
  public boolean offerLast(E e) {
    addLast(e);
    return queue[tail].equals(e);
  }

  @Override
  public E removeFirst() {
    if (isEmpty()) {
      return null;
    }
    Object result = queue[head];
    if (head == capacity - 1) {
      queue[head] = null;
      head = 0;
    } else {
      queue[head] = null;
      head = head + 1;
    }
    size--;
    return (E) result;
  }

  @Override
  public E removeLast() {
    if (isEmpty()) {
      return null;
    }
    Object result = queue[tail];
    if (tail == 0) {
      queue[tail] = null;
      tail = capacity - 1;
    } else {
      queue[tail] = null;
      tail = tail - 1;
    }
    size--;
    return (E) result;
  }

  @Override
  public E pollFirst() {
    E result = getFirst();
    queue[head] = null;
    size--;
    return result;
  }

  @Override
  public E pollLast() {
    E result = getLast();
    queue[tail] = null;
    size--;
    return result;
  }

  @Override
  public E getFirst() {
    return (E) queue[head];
  }

  @Override
  public E getLast() {
    return (E) queue[tail];
  }

  private void ensureCapacity() {
    int headLength = queue.length - head;
    int tailLength = tail + 1;
    capacity += capacity >> 1;
    Object[] newQueue = new Object[capacity];
    System.arraycopy(queue, head, newQueue, capacity - headLength, headLength);
    System.arraycopy(queue, 0, newQueue, 0, tailLength);
    queue = newQueue;
    head = capacity - headLength;
  }

  private void fixOverflowCapacity() {
    if (size == capacity) {
      ensureCapacity();
    }
  }

  @Override
  public E peekFirst() {
    return null;
  }

  @Override
  public E peekLast() {
    return null;
  }

  @Override
  public boolean removeFirstOccurrence(Object o) {
    return false;
  }

  @Override
  public boolean removeLastOccurrence(Object o) {
    return false;
  }

  @Override
  public boolean add(E e) {
    return false;
  }

  @Override
  public boolean offer(E e) {
    return false;
  }

  @Override
  public E remove() {
    return null;
  }

  @Override
  public E poll() {
    return null;
  }

  @Override
  public E element() {
    return null;
  }

  @Override
  public E peek() {
    return null;
  }

  @Override
  public void push(E e) {

  }

  @Override
  public E pop() {
    return null;
  }

  @Override
  public boolean remove(Object o) {
    return false;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean addAll(Collection<? extends E> c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return false;
  }

  @Override
  public void clear() {

  }

  @Override
  public boolean contains(Object o) {
    return false;
  }

  @Override
  public int size() {
    return 0;
  }

  @Override
  public Iterator<E> iterator() {
    return null;
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public <T> T[] toArray(T[] a) {
    return null;
  }

  @Override
  public Iterator<E> descendingIterator() {
    return null;
  }

  public static void main(String[] args) {
    MyDeque<Integer> deque = new MyDeque<>();
    deque.addFirst(10);
    deque.addFirst(10);
    deque.addFirst(10);
    deque.addFirst(10);
    deque.addFirst(10);
    deque.addLast(11);
    deque.addLast(11);
    System.out.println(deque.capacity);

    deque.addLast(11);
    deque.addLast(11);
    deque.addLast(11);
    deque.addLast(11);
    deque.addLast(11);
    deque.addLast(11);

    System.out.println(deque.capacity);
    System.out.println(deque.removeFirst());
    System.out.println(deque.removeFirst());
    System.out.println(deque.removeFirst());
    System.out.println(deque.removeFirst());
    System.out.println(deque.removeFirst());
    System.out.println(deque.removeFirst());
    System.out.println(deque.removeFirst());
    System.out.println(deque.removeFirst());
    System.out.println(deque.removeFirst());
    System.out.println(deque.removeFirst());
    System.out.println(deque.removeFirst());
    System.out.println(deque.removeFirst());
    System.out.println(deque.removeFirst());
    System.out.println(deque.removeFirst());
  }
}
