package com.epam.courses.model;

import com.epam.courses.model.binarytree.BinaryTree;
import com.epam.courses.model.deque.MyDeque;
import com.epam.courses.model.priorityqueue.PriorityQueue;

public class BuisnessLogic {
  private static PriorityQueue priorityQueue;
  private static MyDeque myDeque;
  private static BinaryTree binaryTree;

  public BuisnessLogic() {
    priorityQueue = new PriorityQueue();
    myDeque = new MyDeque();
    binaryTree = new BinaryTree();
  }

  public void addElementToPriorityQueue(Comparable element) {
    priorityQueue.add(element);
  }

  public Comparable peekElementToPriorityQueue() {
   return priorityQueue.peek();
  }

  public Comparable poolElementToPriorityQueue() {
    return priorityQueue.poll();
  }
}
