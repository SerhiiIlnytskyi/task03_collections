package com.epam.courses.model.priorityqueue;

import java.util.AbstractQueue;
import java.util.Arrays;
import java.util.Iterator;

public class PriorityQueue<E extends Comparable<E>> extends AbstractQueue<E> {

  private int capacity = 10;
  private int size = 0;

  private E[] heap;

  public PriorityQueue() {
    heap = (E[]) new Comparable[capacity];
  }

  @Override
  public Iterator<E> iterator() {
    return null;
  }

  @Override
  public int size() {
    return size;
  }

  private int getLeftChildIndex(int parentIndex) {
    return 2 * parentIndex + 1;
  }

  private int getRightChildIndex(int parentIndex) {
    return 2 * parentIndex + 2;
  }

  private int getParentIndex(int childIndex) {
    return (childIndex - 1) / 2;
  }

  private boolean hasLeftChild(int index) {
    return getLeftChildIndex(index) < size;
  }

  private boolean hasRightChild(int index) {
    return getRightChildIndex(index) < size;
  }

  private boolean hasParent(int index) {
    return getParentIndex(index) >= 0;
  }

  private E leftChild(int index) {
    return heap[getLeftChildIndex(index)];
  }

  private E rightChild(int index) {
    return heap[getRightChildIndex(index)];
  }

  private E parent(int index) {
    return heap[getParentIndex(index)];
  }

  private void swap(int indexOne, int indexTwo) {
    E temp = heap[indexOne];
    heap[indexOne] = heap[indexTwo];
    heap[indexTwo] = temp;
  }

  private void ensureCapacity() {
    if (size == capacity) {
      capacity += capacity >> 1;
      heap = Arrays.copyOf(heap, capacity);
    }
  }

  @Override
  public E peek() {
    if (isEmpty()) {
      return null;
    } else {
      return heap[0];
    }
  }

  @Override
  public E poll() {
    if (isEmpty()) {
      return null;
    } else {
      E item = heap[0];
      heap[0] = heap[--size];
      lowerDown();
      return item;
    }
  }

  @Override
  public boolean add(E e) {
    return offer(e);
  }

  @Override
  public boolean offer(E e) {
    ensureCapacity();
    heap[size++] = e;
    liftUp();
    return heap[size - 1].compareTo(e) == 0;
  }

  private void liftUp() {
    int index = size - 1;
    while (hasParent(index) && parent(index).compareTo(heap[index]) > 0) {
      swap(getParentIndex(index), index);
      index = getParentIndex(index);
    }
  }

  private void lowerDown() {
    int index = 0;
    while (hasLeftChild(index)) {
      int smallerChildIndex = getLeftChildIndex(index);
      if (hasRightChild(index) && rightChild(index).compareTo(leftChild(index)) < 0) {
        smallerChildIndex = getRightChildIndex(index);
      }

      if (heap[index].compareTo(heap[smallerChildIndex]) < 0) {
        break;
      } else {
        swap(index, smallerChildIndex);
      }
      index = smallerChildIndex;
    }
  }

  public boolean isEmpty() {
    return size == 0;
  }

  public static void main(String[] args) {
    PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
    priorityQueue.add(29);
    priorityQueue.add(31);
    priorityQueue.add(37);
    priorityQueue.add(26);
    priorityQueue.add(26);
    priorityQueue.add(34);
    priorityQueue.add(15);
    System.out.println(priorityQueue.peek());
    System.out.println(priorityQueue.poll());
    System.out.println(priorityQueue.poll());
    System.out.println(priorityQueue.peek());
    System.out.println(priorityQueue.peek());
    System.out.println(priorityQueue.poll());
    System.out.println(priorityQueue.poll());
    System.out.println(priorityQueue.poll());
    System.out.println(priorityQueue.poll());
    System.out.println(priorityQueue.poll());
    System.out.println(priorityQueue.poll());
    System.out.println(priorityQueue.poll());
    System.out.println(priorityQueue.poll());
    System.out.println(priorityQueue.poll());
  }
}
