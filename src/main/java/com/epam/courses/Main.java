package com.epam.courses;

import com.epam.courses.view.MainView;

public class Main {

  public static void main(String[] args) {
    new MainView().show();
  }
}
