package com.epam.courses.view;

public interface Printable {
  void print();
}
