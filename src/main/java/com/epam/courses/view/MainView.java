package com.epam.courses.view;

import com.epam.courses.controller.BinaryTreeController;
import com.epam.courses.controller.DequeController;
import com.epam.courses.controller.PriorityQueueController;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainView {
  private static Scanner INPUT = new Scanner(System.in);
  private BinaryTreeController binaryTreeController;
  private DequeController dequeController;
  private PriorityQueueController priorityQueueController;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;

  public MainView() {
    binaryTreeController = new BinaryTreeController();
    dequeController = new DequeController();
    priorityQueueController = new PriorityQueueController();
    menu = new LinkedHashMap<>();
    menu.put("1", " 1 - Add to priority queue element");
    menu.put("2", " 2 - Peek element from priority queue");
    menu.put("3", " 3 - Poll element from priority queue");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
  }

  private void pressButton1() {
    System.out.println("Please enter new element to add it to priority queue");
    String element = INPUT.nextLine();
    priorityQueueController.addElementToPriorityQueue(element);
  }

  private void pressButton2() {
    System.out.println(priorityQueueController.peekElementToPriorityQueue());
  }

  private void pressButton3() {
    System.out.println(priorityQueueController.poolElementToPriorityQueue());
  }

  private void pressButton0() {
    System.out.println("Good luck");
    INPUT.close();
    System.exit(0);
  }

  private void outputMenu() {
    System.out.println("\nMENU:");
    menu.values().forEach(System.out::println);
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println(" Q - quit");
      System.out.print("Please, select menu point: ");
      keyMenu = INPUT.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
    this.pressButton0();
  }
}
